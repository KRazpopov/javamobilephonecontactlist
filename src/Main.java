import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone("0039887878778");

    public static void main(String[] args){

        boolean quit = false;
        startPhone();
        printActions();
        while(!quit){
            System.out.println("Enter action  6 to show available actions" );
            int action = scanner.nextInt();
            scanner.nextLine();
            switch(action){
                case 0:
                    System.out.println( "Shutting down");
                    quit = true;
                    break;
                case 1:mobilePhone.printContacts();
                break;
                case 2:
                    addNewContact();
                    break;
                case 3:updateContact();
                break;
                case 4: removeContact();
                break;
                case 5: queryContact();
                break;
                case 6:printActions();
                break;
             }


        }

    }
    private static void updateContact(){
        System.out.println("enter existing contact name");
        String name = scanner.nextLine();
        Contact existingRecord = mobilePhone.queryContact(name);
        if(existingRecord==null){
            System.out.println("Contact not found");
            return;
        }
        System.out.println("Enter new contact name");
        String newName = scanner.nextLine();
        System.out.println("Enter new contact phone number");
        String newnumber = scanner.nextLine();
        Contact newContact = Contact.createContact(newName,newnumber);
        if(mobilePhone.updateContact(existingRecord,newContact)){
            System.out.println("successfully updated contact");
        }
        else {
            System.out.println("Error updating record");
        }

    }

    private static void removeContact() {
        System.out.println("enter existing contact name");
        String name = scanner.nextLine();
        Contact existingRecord = mobilePhone.queryContact(name);
        if (existingRecord == null) {
            System.out.println("Contact not found");
            return;
        }
       if( mobilePhone.removeContact(existingRecord)){
           System.out.println("contact removed successfully");
       }
       else{
           System.out.println("Error deleting");
       }
    }
    private static void addNewContact(){
        System.out.println( "Enter contact name");
        String name = scanner.nextLine();
        System.out.println("Enter Phone number");
        String phoneNumber = scanner.nextLine();
        Contact newContact = Contact.createContact(name,phoneNumber);
        if(mobilePhone.addNewcontact(newContact)){
            System.out.println("New contact "+name +"phone"+phoneNumber+"added successfully");
        }
        else{
            System.out.println("Name already on file, could not be set");
        }
    }

    private static void queryContact() {
        System.out.println("enter existing contact name");
        String name = scanner.nextLine();
        Contact existingRecord = mobilePhone.queryContact(name);
        if (existingRecord == null) {
            System.out.println("Contact not found");
            return;
        }
        System.out.println("Name"+existingRecord.getName()+"phone number"+existingRecord.getPhonenumber());
    }


    private static void startPhone(){
        System.out.println("Starting");
    }

    private static void printActions(){

        System.out.println("\nAvailable Actions:\n press:");
        System.out.println("O - to shut down\n"+
                            "1 - print contacts\n"+
                            "2 - add a new contact\n"+
                             "3 - update contact\n"+
                "4 - remove existing contact\n"+
                "5 - query if an existing contact exists\n"+
                "6 - print a list of all actions");
        System.out.println( "Choose your action: ");
            }
}
