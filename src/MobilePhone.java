import java.util.ArrayList;

public class MobilePhone {
    private String myNumber;
    private ArrayList<Contact> myContact;///list of Contact instances


    public MobilePhone(String myNumber){
        this.myNumber = myNumber;
        this.myContact = new ArrayList<Contact>();
    }

    public boolean addNewcontact(Contact contact){
        if(findContact(contact.getName())>=0){
            System.out.println("Contact is already added");
            return false;
        }
        myContact.add(contact);
        return true;
    }

    public  boolean removeContact(Contact contact){
        int foundPOsition = findContact(contact);
        if(foundPOsition<0){
            System.out.println(contact.getName()+" was not found ");
            return false;
        }
        this.myContact.remove(foundPOsition);
        System.out.println(contact.getName()+ " was deleted");
        return true;
    }

    private int findContact(Contact contact){
        return this.myContact.indexOf(contact);
        ///returns 0->1,2,3 if existing in the lyst and negative(-1,-2) if not existing

    }

    private int findContact(String contactName){
        for(int i=0;i<this.myContact.size();i++){
            Contact contact = this.myContact.get(i);///temp Contact instance
            if(contact.getName().equals(contactName)){
                return i;
            }
        }
        return -1;
    }

    public boolean updateContact(Contact oldContact,Contact newContact){
        int foundPOsition = findContact(oldContact);
        if(foundPOsition<0){
            System.out.println(oldContact.getName()+" was not found ");
            return false;
        }
        this.myContact.set(foundPOsition,newContact);
        System.out.println(oldContact.getName()+ " was replaced with "+newContact.getName() );
        return true;
    }

    public String queryContact(Contact contact){
        if(findContact(contact)>=0){
            return contact.getName();
        }
        return null;
    }

    public void printContacts(){
        for(int i=0;i<myContact.size();i++){
            System.out.println(i+1+" ."+this.myContact.get(i).getName()+"->"+
            this.myContact.get(i).getPhonenumber());

        }

    }

    public Contact queryContact(String name){
      int position = findContact(name);
      if(position>=0){
          return this.myContact.get(position);
      }
      return null;
    };




}
